# Einführung in TEI

Mathias Göbel (@goebel_m) und Uwe Sikora

![SUB Göttingen](img/sub-logo.png) <!-- .element style="width: 340px;" -->

diese Slides: https://tei.pages.gwdg.de/slides/talks/2020-12-08/

---

Programm und Slides auf Basis der Vorträge bei #dhoxss.

Definition aus oder in Anlehnung an Wikipedia und TEI. 

---

- Texte und Dokumente
- Definitionen (Markup, Encoding, Annotation)

---

- TEI
  - Historie
  - Kernkonzepte
  - Module
  - Richtlinien

---

- Metadaten-Auszeichnung
- Text-Auszeichnung

---

- projektspezifische Anpassung

---

- Werkzeuge
- Ressourcen

|||


![Fontane Notizbücher. D06_013.jpg.](https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:1903k.1/full/600,/0/default.jpg)

<small>TextGrid Repository (2016). Notizbuch D06 JPEG Images. D06_013.jpg. Fontane Notizbücher. Staatsbibliothek zu Berlin - Preußischer Kulturbesitz, Handschriftenabteilung. https://hdl.handle.net/11378/0000-0005-F9CF-A</small>

---

![twitter tweet](img/Screenshot_2020-12-07%20Tweets%20mit%20Antworten%20von%20DARIAH-EU%20(%20DARIAHeu)%20Twitter.png)

---

![Vorwärts. In: Wikipedia.](https://upload.wikimedia.org/wikipedia/de/1/18/Vorw%C3%A4rts_Rathenau.jpg)

|||

## Definitionen I

---

### Texte ≠ Dokument
Texte sind Abstraktionen der Dokumente, erstellt von oder für eine Zielgruppe. Diese Texte können wir mit Markup versehen/erweitern.

---

### Markup

= Textauszeichnung

Verwendung einer (maschinenlesbaren) Sprache, als Ergänzung eines Textes dienend. Mit dieser Sprache werden Strukturen, Eigenschaften, Zugehörigkeiten und Darstellungsformen beschrieben.

<small>(in Anlehnung an Wikipedia "Auszeichnungssprache")</small>

---

### Annotation

Eine einzelne Auszeichnung an einer einzelnen Stelle wir als eine Annotation genannt.
Oft auch synonym zu _Markup_ verwendet.
Oft auch für (externe) Anmerkungen/Kommentare zu einer bestimmten Quelle verwendet.

---

### Encoding

Die Gesamtheit einzelner Textauszeichnungen in einem Dokument oder Sammlung von Dokumenten (z. Bsp. Editon, Korpus, etc.)

---

### Schema

„eine formale Beschreibung der Struktur von Daten“ (Wikipedia: Schema (Informatik))

Ein Schema legt die Möglichkeiten der Auszeichnung fest. Es beschreibt, wie das Markup aussehen darf, beinhaltet aber auch Definitionen der verwendeten Befriffe (Elementnamen).

---

![Vorwärts. In: Wikipedia.](https://upload.wikimedia.org/wikipedia/de/1/18/Vorw%C3%A4rts_Rathenau.jpg)

---

Rathenau ermordet!
Um ¼ 12 Uhr teilte der Reichskanzler den in der Wandelhalle des 
Reichstags versammelten Abgeordneten mit, daß vor etwa einer halben Stunde
der Außenminister Dr. Rathenau ermordet worden sei.
Als Dr. Rathenau heute vormittag 11 Uhr sein Automobil vor seinem [...]

---

```xml
<ÜBERSCHRIFT>Rathenau ermordet!</ÜBERSCHRIFT> 
<ABSATZ>Um ¼ 12 Uhr teilte der Reichskanzler den in der Wandelhalle des 
Reichstags versammelten Abgeordneten mit, daß vor etwa einer halben Stunde
der Außenminister Dr. Rathenau ermordet worden sei.</ABSATZ>
<ABSATZ>Als Dr. Rathenau heute vormittag 11 Uhr sein Automobil vor seinem [...]
</ABSATZ>
```

---

### Vorteile

---

#### Generalisierbarkeit

Markup dient dazu Inhalt und Form zu trennen. Durch diese Generalisierung lassen sich
verschiedene Formate aus einer Datengrundlage erstellen: Webseite, PDF-Dokument (und
damit schließlich auch Druckausgaben)

---

#### Erweiterbarkeit

Es können weitere Annotationen hinzugefügt werden.

---

#### Maschinenlesbarkeit

Markup kann von Maschinen auf unterschiedliche Weise verarbeitet werden.
- Aggregation
- übergreifende Suchfunktionen
- virtuelle Kollektionen
- …

---

```xml
<ÜBERSCHRIFT>Rathenau ermordet!</ÜBERSCHRIFT> 
<ABSATZ>Um ¼ 12 Uhr teilte der Reichskanzler den in der Wandelhalle des 
Reichstags versammelten Abgeordneten mit, daß vor etwa einer halben Stunde
der Außenminister Dr. Rathenau ermordet worden sei.</ABSATZ>
<ABSATZ>Als Dr. Rathenau heute vormittag 11 Uhr sein Automobil vor seinem [...]
</ABSATZ>
```

|||

# TEI

---

## Historie der TEI

- seit 1987
- 1987? <!-- .element class="fragment" -->
- Das ist ungefähr 33 Jahr her! <!-- .element class="fragment" -->

---

![TEI founding at Vassar College](https://mathias-goebel.github.io/2015-06-teisimple/img/dhoxss-slide85.png)

---

- Seit 1987 entwickelt
- eine Fachöffentlichkeit (“Community”) <!-- .element class="fragment" -->
- ein Rahmenwerk, um Text digital erschließen zu können. <!-- .element class="fragment" -->

Dabei wurden seither 550 solcher „Elemente“ konstruiert. <!-- .element class="fragment" -->

---

## Kernkonzepte

- **Module**: TEI besteht aus unterschiedlichen Modulen
- **Element- und Attributsklassen**: Die einzelnen Module bestehen aus Deklarationen für Elemente, Attribute und Inhaltsmodelle

**Diesem modularen Aufbau hat das TEI encoding-scheme seine Felixibilität zu verdanken!**

---

## TEI-Module

- Die Module der TEI-Welt sind inhaltlich organisiert
- Module können frei miteinander kombiniert werden, ganz nach den Anforderungen der Texterschließung

---

- 4 Module sind obligatorisch:
  - "**tei**": grundlegende TEI-Infrastruktur 
  - "**core**": häufig gebrauchte Elemente und Attribute (z.B.: tei:p, tei:note, tei:list)
  - "**header**": Metadaten einer TEI-Ressource (z.B.: tei:titleStmt)
  - "**textstructure**": Grundlegende Strukturen einer TEI-Ressource (z.B.: tei:text)

---

- 21 Module insgesamt:
  - "**dictionaries**" : Modul zum Encoding von Wörterbüchern
  - "**vers**" : Modul zum Encoding von Phänomenen in lyrischen Texten (z.B.: Verstrukturen)
  - "**namesdates**" : Modul zum Encoding von Eigennamen, Daten, Personen und Orten
  - "**drama**": Modul zum Encoding von Dramentexten 

- Beschrieben in den TEI-Richtlinien

---

![tei elemente nach modulen](https://mathias-goebel.github.io/2015-06-teisimple/img/tei-elements.png)

---

## TEI-Richtlinien:
Bekannt als die "[P5-Guidelines](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html)"

![TEI-P5](img/tei-guidelines.png)

---

![TEI-P5 Sample](img/tei-guidelines-sample.png)

---

## Dokumentation von Elementen, Attributen etc.

![TEI-P5 Element Beispiel](img/tei-performance.png)

---

![TEI-P5 Element Beispiel](img/tei-performance_2.png)

|||

# TEI-Markup

---

## TEI: so sieht's (grundsätzlich) aus ...

```xml
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
      ...
  </teiHeader>
  <text>
      <body>
         ...
      </body>
  </text>
</TEI>
```

---

Grundsätzliche Aufteilung des XML-Dokumentes in
- `teiHeader`
- `text`

|||

## Metadaten Bereich: Der TEI-Header
  - `teiHeader`
  - Metadaten der TEI-Datei und der erschlossen Quellen:
    - Titel, Autoren, Editoren, Veröffentlichungsvermerke
    - Handschriftenbeschreibung
    - Provenienzen
    - Textkritik

---

## TEI-Header: Grundgerüst

```xml
<teiHeader>
    <fileDesc>
        <titleStmt>
            [...]
        </titleStmt>
        <publicationStmt>
            [...]
        </publicationStmt>
        <sourceDesc>
            [...]
         </sourceDesc>
    </fileDesc>
</teiHeader>
```

---

### Titelvermerk
```xml
<titleStmt>
    <title xml:lang="en">The Magnificent Parchment</title>
    <title type="alt" xml:lang="heb">היריעה המפוארת</title>
    <editor>
        <name xml:lang="en">H. Muster</name>
    </editor>
    <funder>
        Volkswagen Stiftung Niedersächsisches Vorab - Research Cooperation Lower Saxony – Israel  Maps of God - Building a Portal to Visual Kabbalah (Digital Humanities)
    </funder>
    <respStmt>
        <resp>TEI-P5 Application Profile, Metadata and Dataconversion</resp>
        <name xml:lang="en">Uwe Sikora</name>
    </respStmt>
</titleStmt>
```

---

### Veröffentlichungsvermerk
```xml
<publicationStmt>
    <publisher>The Ilanot Project, University of Haifa</publisher>
    <publisher>University of Göttingen</publisher>
    <availability>
        <licence target="https://creativecommons.org/licenses/by-sa/3.0">This resource is published under the CC BY-SA 3.0 licence.</licence>
    </availability>
</publicationStmt>

```

---

### Handschriftenbeschreibung

```xml
<msDesc>
    <head>
        <title xml:lang="en">The Magnificent Parchment</title>
    </head>
    <msIdentifier>
        <country key="GB-ENG">England</country>
        <settlement>Oxford</settlement>
        <repository xml:lang="en">Bodleian Libraries, University of Oxford</repository>
        <idno>MS Hunt. Add. D</idno>
        <altIdentifier>
            <idno>Neubauer 1949</idno>
        </altIdentifier>
        <msName xml:lang="en">The Magnificent Parchment</msName>
        <msName xml:lang="heb" type="alt">היריעה המפוארת</msName>
    </msIdentifier>
    <msContents>
        <textLang mainLang="he-x-rabbinic"/>
        <summary xml:lang="en">Oxford - Bodleian Library MS Hunt. Add. D (Neubauer 1949) is a particularly fine, early, and well-preserved witness of a family of large manuscript rotuli ("ilanot") to which we have given the name "The Magnificent Parchment." Only one witness (London - British Library Or. 6465 Scroll) carries a colophon, dating it to 1556; the ur-roll was probably crafted c.1500 in northern Italy. This family of "luxury" manuscripts is distinguished by its large size, its graphical richness, and its roughly 33000 word anthological text.</summary>
    </msContents>
</msDesc>
```

---

```xml
<physDesc>
    <objectDesc form="scroll">
        <supportDesc material="parchment">
            <support>Parchment</support>
            <extent>
                <measure>5 leaves</measure>
                <dimensions unit="cm">
                    <height quantity="200">200</height>
                    <width quantity="58">58</width>
                </dimensions>
            </extent>
        </supportDesc>
    </objectDesc>
</physDesc>
```

---

```xml
<history>
    <origin notBefore="1550" notAfter="1560" cert="high">...</origin>
    <acquisition when="1949">
      <p>Recorded in one catalogue of the books belonging to Bodleian Library.</p>
    </acquisition>
</history>
```

---

```xml
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
      ...
  </teiHeader>
  <text>
      <body>
         ...
      </body>
  </text>
</TEI>
```

|||

## Text-Daten Bereich

---

Faksimiles und Textkörper

---

### Textkörper

`<text>…</text>`

---

#### `text`

…enthält einen einzelnen, eigenständigen oder kompilierten Text, zum Beispiel ein Gedicht oder Drama, eine Sammlung von Aufsätzen, einen Roman, ein Wörterbuch oder ein Korpus-Sample.

---

#### Unterteilung

- `<front/>`: vorangestellten Texte (Titelseite, Vorworte, Widmungen, usw.)
- `<body/>`: eigenständiger Text
- `<back/>`: Anhänge jeglicher Art

---

```xml
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
<!-- ... -->
 </teiHeader>
 <text>
  <front>
<!-- front matter of copy text, if any, goes here -->
  </front>
  <body>
<!-- body of copy text goes here -->
  </body>
  <back>
<!-- back matter of copy text, if any, goes here -->
  </back>
 </text>
</TEI>
```

---

#### weitere Unterteilung

innerhalb der drei Optionen `front`, `body`, `back`:
- `div`
- `head`
- `p`

---

```xml
<head>
  Rathenau ermordet!
</head>

<p>
  Um ¼ 12 Uhr teilte der Reichskanzler den in der Wandelhalle des 
  Reichstags versammelten Abgeordneten mit, daß vor etwa einer halben Stunde
  der Außenminister Dr. Rathenau ermordet worden sei.
</p>
<p>
  Als Dr. Rathenau heute vormittag 11 Uhr sein Automobil vor seinem […]
</p>
```

---

### Semantische Auszeichnung

- Personen <!-- .element class="fragment" -->
- Orte <!-- .element class="fragment" -->
- Zeitangaben <!-- .element class="fragment" -->
- Referezen <!-- .element class="fragment" -->
- Events <!-- .element class="fragment" -->
- Organsationen <!-- .element class="fragment" -->

---

```xml
<head>
  <person>Rathenau</person> ermordet!
</head>

<p>
  Um <date when="1922-06-24-11:15CET">¼ 12 Uhr</date> teilte
  der <person>Reichskanzler</person> den in der <place>Wandelhalle des 
  Reichstags</place> versammelten Abgeordneten mit,
  daß <date when="1922-06-24-10:45CET">vor etwa einer halben Stunde</date>
  der Außenminister Dr. Rathenau ermordet worden sei.
</p>
```

---

### Faksimiles

topografische Auszeichnung

`faksimile`, `surfaceGrp`, `surface`, `graphic` 

---

![topographic structure](https://www.tei-c.org/release/doc/tei-p5-doc/de/html/Images/facs-fig1.png)

<!-- .element style="height: 300px;" -->

---

```xml
<facsimile>
 <surface ulx="0" uly="0" lrx="200" lry="300">
  <graphic url="Bovelles-49r.png"/>
  <zone ulx="25" uly="25" lrx="180" lry="60">
<!-- contains the title -->
  </zone>
  <zone ulx="28" uly="75" lrx="175" lry="178"/>
<!--  contains the paragraph in italics -->
  <zone ulx="105" uly="76" lrx="175"
   lry="160"/>
<!-- contains the figure -->
  <zone ulx="45" uly="125" lrx="60" lry="130"/>
<!-- contains the word "pendans" -->
 </surface>
</facsimile>
```

---

### integrierte Transkription

```xml
<facsimile>
 <surface ulx="0" uly="0" lrx="200" lry="300">
  <graphic url="Bovelles-49r.png"/>
  <zone ulx="25" uly="25" lrx="180" lry="60">
    <line>DU SON ET ACCORD DES CLOCHES ET</line>
    <line>des alleures des
 chevaulx, chariotz &amp; charges, des fontaines:&amp;</line>
 <line>encyclie du monde, &amp; de la dimension du corps humain.</line>
 <line>Chapitre septiesme</line>
  </zone>
</facsimile>
```

|||

## Standard vs. projektspezifische Anpassung

---

### Ein paar Begriffe zur Klärung
  - "**Standard**" : Eine einheitliche, konsenbasierte Art und Weise, Einheiten aus einer bestimmten Domäne formalisiert zu beschreiben.
  - "**Domäne**" : Ein (Wissens-)gebiet, über das Aussagen getroffen wird
  - "**Anwendungsprofil**" : Eine konkrete Modellableitung bzw. -ausformung basierend auf einem Standard, die in der Regel auf Anforderungen einer Softwareanwendung reagiert, daher *Anwendungs*profil

---

### Die TEI-XML Welt
  - "**TEI-all**": Der "Standard" aka. das Referenzmodell
  - Neben TEI-all gibt es auch konkrete TEI-APs
    - in der TEI-Welt als "_Customizations_" bezeichnet
    - "TEI-lite", "-bare", "-simplePrint", ...
  - Ein Überblick über alle TEI-Customizations findet sich [hier](https://tei-c.org/guidelines/Customization/).

---

### Was ist "ODD"?
  - _"**O**ne **D**ocument **D**oes it all!"_
  - Dokumentationsformat zur Definition und Dokumentation von (eigenen) TEI-APs:
    - TEI-Datei
    - Grundlage zur Erstellung von Validierungsschemas (RelaxNG, XML-Schema, Schematron etc.)
  - Ausführlich beschrieben in den [TEI-Guidelines, §22](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/TD.html)

---

### Warum "ODD"?
  - "[...] it is almost impossible to use the TEI schema without customizing it in some way." [TEI-Guidelines, §23](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/USE.html#MD)
  - Jede wissenschaftliche Beschäftigung mit einem Gegenstand bringt die Notwendigkeit mit sich, festzulegen, was auf welche Weise beschrieben werden soll.

---

**Resultat**: Die Erarbeitung eines formalen Textmodells für eben jene Texte, die erschlossen werden sollen.

|||

## Tools

- XML-Editor
- [Roma](https://roma.tei-c.org/)

---

## Korpora

- [DTA](http://www.deutschestextarchiv.de) / 
- [DraCor](https://dracor.org)
- [TextGrid Repository](https://textgridrep.org/)

---

## Ressourcen
- [TEI-C](https://tei-c.org/)
- [TEI-Support](https://tei-c.org/Support/)
- [Wiki](https://wiki.tei-c.org)
- [TEI by example](https://teibyexample.org/)
- [XML-Kurzreferenzen des ide](https://www.i-d-e.de/publikationen/weitereschriften/xml-kurzreferenzen/)

|||

`</TEI>`
